package org.example.framework.server.handler;

import org.example.framework.security.http.MediaTypes;
import org.example.framework.server.http.HttpCodes;
import org.example.framework.server.http.RequestImpl;
import org.example.framework.server.http.Response;

import java.io.IOException;

@FunctionalInterface
public interface Handler {

    void handle(final RequestImpl request, final Response response) throws IOException;

    static void internalServerError(final RequestImpl request, final Response response) throws IOException {
        response.write(HttpCodes.INTERNAL_SERVER_ERROR.codeValue(),
                HttpCodes.INTERNAL_SERVER_ERROR.messageValue(),
                MediaTypes.TEXT_PLAIN,
                "Something Bad Happened");
    }

    static void notFoundHandler(final RequestImpl request, final Response response) throws IOException {
        response.write(HttpCodes.RESOURCE_NOT_FOUND.codeValue(),
                HttpCodes.RESOURCE_NOT_FOUND.messageValue(),
                MediaTypes.TEXT_PLAIN,
                "Resource not found");
    }

    static void methodNotAllowedHandler(final RequestImpl request, final Response response) throws IOException {
        response.write(HttpCodes.METHOD_NOT_ALLOWED.codeValue(),
                HttpCodes.METHOD_NOT_ALLOWED.messageValue(),
                MediaTypes.TEXT_PLAIN,
                "Method Not Allowed");
    }

    static void unauthorized(final RequestImpl request, final Response response) throws IOException {
        response.write(HttpCodes.UNAUTHORIZED.codeValue(),
                HttpCodes.UNAUTHORIZED.messageValue(),
                MediaTypes.TEXT_PLAIN,
                "unauthorized");
    }

    static void badRequest(final RequestImpl request, final Response response) throws IOException {
        response.write(HttpCodes.BAD_REQUEST.codeValue(),
                HttpCodes.BAD_REQUEST.messageValue(),
                MediaTypes.TEXT_PLAIN,
                "Bad Request");
    }

}
