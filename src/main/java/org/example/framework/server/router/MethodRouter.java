package org.example.framework.server.router;

import org.example.framework.security.http.HttpMethods;
import org.example.framework.server.exception.AmbiguousMappingException;
import org.example.framework.server.util.Pair;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MethodRouter {
    private final Map<Pattern, Map<HttpMethods, Pair<Object, Method>>> routes;

    public MethodRouter() {
        routes = new HashMap<>();
    }

    private MethodRouter(Map<Pattern, Map<HttpMethods, Pair<Object, Method>>> routes) {
        this.routes = routes;
    }

    public synchronized void register(HttpMethods httpMethod, Pattern pattern, Object bean, Method method) {
        final Class<?> clazz = bean.getClass();
        final Map<HttpMethods, Pair<Object, Method>> httpMethodsPairMap = routes.computeIfAbsent(pattern, o -> new HashMap<>());
        if (httpMethodsPairMap.containsKey(httpMethod)) {
            throw new AmbiguousMappingException(
                    "try to map controller " + clazz.getName() +
                            " method " + method.getName() +
                            " to " + method.getName() + " path " + pattern.pattern()
            );
        }
        httpMethodsPairMap.put(httpMethod, new Pair<>(bean, method));
    }

    public synchronized Optional<MethodRoute> findController(final String requestPath, final String requestMethod) {
        for (final Map.Entry<Pattern, Map<HttpMethods, Pair<Object, Method>>> entry : routes.entrySet()) {
            final Matcher matcher = entry.getKey().matcher(requestPath);
            if (!matcher.matches()) {
                continue;
            }

            for (final Map.Entry<HttpMethods, Pair<Object, Method>> controllers : entry.getValue().entrySet()) {
                final HttpMethods httpMethod = controllers.getKey();
                final Object controller = controllers.getValue().getFirst();
                final Method method = controllers.getValue().getSecond();

                if (httpMethod.name().equals(requestMethod)) {
                    return Optional.of(MethodRoute.builder()
                            .object(controller)
                            .method(method)
                            .matcher(matcher)
                            .build());
                }
            }
        }

        return Optional.empty();
    }

    public static MethodRouter of(final Map<Pattern, Map<HttpMethods, Pair<Object, Method>>> routes) {
        return new MethodRouter(routes);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        MethodRouter that = (MethodRouter) other;

        if (routes == null) {
            return that.routes == null;
        }

        if (routes.size() != that.routes.size()) {
            return false;
        }
        for (final Map.Entry<Pattern, ?> entry : routes.entrySet()) {
            final String pattern = entry.getKey().pattern();
            final Object expectedPatternRoutes = entry.getValue();
            final Object actualPatternsRoutes = that.routes.entrySet().stream()
                    .filter(o -> o.getKey().pattern().equals(pattern))
                    .findFirst()
                    .map(Map.Entry::getValue)
                    .orElse(null);

            if (!Objects.equals(expectedPatternRoutes, actualPatternsRoutes)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        return routes != null ? routes.hashCode() : 0;
    }
}
