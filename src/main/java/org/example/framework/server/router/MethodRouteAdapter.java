package org.example.framework.server.router;

import lombok.RequiredArgsConstructor;
import org.example.framework.server.controller.method.handler.ReturnValueHandler;
import org.example.framework.server.controller.method.resolver.ArgumentResolver;
import org.example.framework.server.exception.RequestProcessException;
import org.example.framework.server.handler.Handler;
import org.example.framework.server.http.RequestImpl;
import org.example.framework.server.http.Response;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class MethodRouteAdapter implements Handler {
    private final MethodRoute route;
    private final List<ArgumentResolver> argumentResolvers;
    private final List<ReturnValueHandler> returnValueHandlers;

    @Override
    public void handle(final RequestImpl request, final Response response) {
        final Method method = route.getMethod();
        final Object controller = route.getObject();
        try {
            final List<Object> args = new ArrayList<>(method.getParameterCount());
            for (final Parameter parameter : method.getParameters()) {
                for (final ArgumentResolver argumentResolver : argumentResolvers) {
                    if (argumentResolver.supportsParameter(parameter)) {
                        args.add(argumentResolver.resolveArgument(parameter, request));
                        break;
                    }
                }
            }
            final Object result = method.invoke(controller, args.toArray());
            for (final ReturnValueHandler returnValueHandler : returnValueHandlers) {
                if (returnValueHandler.supportsReturnType(method)) {
                    returnValueHandler.handleReturnType(result, method, request, response);
                    break;
                }
            }

        } catch (Exception e) {
            throw new RequestProcessException(e);
        }
    }
}
