package org.example.framework.server.router;

import lombok.Builder;
import lombok.Data;

import java.lang.reflect.Method;
import java.util.regex.Matcher;

@Builder
@Data
public class MethodRoute {
    private final Object object;
    private final Method method;
    private final Matcher matcher;
}
