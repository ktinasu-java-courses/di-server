package org.example.framework.server.http;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.framework.security.http.HttpHeaders;
import org.example.framework.security.http.MediaTypes;
import org.example.framework.security.request.Request;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class RequestImpl implements Request {
    private String method;
    private String path;
    private Matcher pathMatcher;
    private String query;
    private Map<String, List<String>> queryParams = new LinkedHashMap<>();
    private String httpVersion;
    private Map<String, String> headers = new LinkedHashMap<>();
    private byte[] body;
    private Map<String, List<String>> formParams = new LinkedHashMap<>();

    public void setHeader(String key, String value) {
        headers.put(key, value);
    }

    public String getPathGroup(String name) {
        return pathMatcher.group(name);
    }

    public String getPathGroup(int index) {
        return pathMatcher.group(index);
    }

    public Optional<String> getQueryParam(final String name) {
        if (!queryParams.containsKey(name)) {
            return Optional.empty();
        } else {
            final List<String> values = queryParams.get(name);
            return Optional.of(values.get(0));
        }
    }

    public List<String> getQueryParams(final String name) {
        return queryParams.getOrDefault(name, null);
    }

    public Map<String, List<String>> getAllQueryParams() {
        return queryParams;
    }

    public Optional<String> getFormParam(final String name) {
        if (!formParams.containsKey(name)) {
            return Optional.empty();
        } else {
            final List<String> values = formParams.get(name);
            return Optional.of(values.get(0));
        }
    }

    public List<String> getFormParams(final String name) {
        return formParams.getOrDefault(name, null);
    }

    public Map<String, List<String>> getAllFormParams() {
        return formParams;
    }

    public MediaTypes getContentType() {
        return Optional.ofNullable(headers.get(HttpHeaders.CONTENT_TYPE.value()))
                .map(MediaTypes::fromValue)
                .orElse(MediaTypes.ANY)
                ;
    }

    public MediaTypes getAccept() {
        return Optional.ofNullable(headers.get(HttpHeaders.ACCEPT.value()))
                .map(MediaTypes::fromValue)
                .orElse(MediaTypes.ANY);
    }
}
