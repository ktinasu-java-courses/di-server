package org.example.framework.server.http;

public enum HttpCodes {
    OK(200, "Ok"),
    INTERNAL_SERVER_ERROR(500, "Internal Server Error"),
    RESOURCE_NOT_FOUND(404, "Resource not found"),
    METHOD_NOT_ALLOWED(405, "Method Not Allowed"),
    UNAUTHORIZED(401, "Unauthorized"),
    BAD_REQUEST(400, "Bad Request");

    private final int codeValue;
    private final String messageValue;

    HttpCodes(int codeValue, String messageValue) {
        this.codeValue = codeValue;
        this.messageValue = messageValue;
    }

    public int codeValue() {
        return codeValue;
    }

    public String messageValue() {
        return messageValue;
    }
}
