package org.example.framework.server.annotation;

import org.example.framework.security.http.HttpMethods;
import org.example.framework.security.http.MediaTypes;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RequestMapping {
  HttpMethods method() default HttpMethods.GET;
  String path() default "";
  MediaTypes produces() default MediaTypes.ANY;
}
