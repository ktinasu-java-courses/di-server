package org.example.framework.server.query.parser;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.server.exception.ParseException;
import org.example.framework.server.http.RequestImpl;
import org.example.framework.server.parser.Parser;
import org.example.framework.server.query.exception.QueryParseException;

import java.util.List;
import java.util.Map;

@Slf4j
public class QueryParser implements Parser {

    public RequestImpl parse(final RequestImpl request) {
        final String query = request.getQuery();
        log.debug("Query to parse: {}", query);
        if (query == null) {
            return request;
        }
        try {
            final Map<String, List<String>> queryMap = Parser.parseString(query);
            log.debug("query params: {}", queryMap);
            request.setQueryParams(queryMap);
        } catch (ParseException e) {
            throw new QueryParseException("Invalid query");
        }

        return request;
    }
}
