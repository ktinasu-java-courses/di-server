package org.example.framework.server.parser;

import org.example.framework.server.exception.ParseException;
import org.example.framework.server.http.RequestImpl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public interface Parser {
    RequestImpl parse(final RequestImpl request);

    static Map<String, List<String>> parseString(String lineToParse) {
        final String[] parts = lineToParse.split("\\&");
        final Map<String, List<String>> map = new LinkedHashMap<>();
        for (String queryPart : parts) {
            final String[] split = new String[2];
            split[0] = queryPart.split("\\=")[0];
            final int splitLength = queryPart.split("\\=").length;
            if (splitLength > 2) {
                throw new ParseException();
            }
            if (splitLength == 1) {
                split[1] = "";
            } else {
                split[1] = queryPart.split("\\=")[1];
            }

            if (!map.containsKey(split[0])) {
                final List<String> values = new ArrayList<>();
                values.add(split[1]);
                map.put(split[0], values);
            } else {
                final List<String> values = map.get(split[0]);
                values.add(split[1]);
                map.replace(split[0], values);
            }
        }
        return map;
    }
}
