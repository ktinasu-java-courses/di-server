package org.example.framework.server.form.exception;

public class FormParseException extends RuntimeException {
    public FormParseException() {
    }

    public FormParseException(String message) {
        super(message);
    }

    public FormParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public FormParseException(Throwable cause) {
        super(cause);
    }

    public FormParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
