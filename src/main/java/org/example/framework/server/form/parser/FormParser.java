package org.example.framework.server.form.parser;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.security.http.HttpHeaders;
import org.example.framework.server.exception.ParseException;
import org.example.framework.server.form.exception.FormParseException;
import org.example.framework.server.http.RequestImpl;
import org.example.framework.server.parser.Parser;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

@Slf4j
public class FormParser implements Parser {
    public RequestImpl parse(final RequestImpl request) {
        boolean hasContentTypeHeader = false;
        final Map<String, String> headers = request.getHeaders();
        for (String header : headers.keySet()) {
            if (!header.equalsIgnoreCase(HttpHeaders.CONTENT_TYPE.value())) {
                continue;
            }
            hasContentTypeHeader = true;
            if (!headers.get(header).equals("application/x-www-form-urlencoded")) {
                return request;
            }
        }
        if (!hasContentTypeHeader) {
            return request;
        }

        if (request.getBody() == null) {
            return request;
        }

        final String body = new String(request.getBody(), StandardCharsets.UTF_8);
        log.debug("form to parse: {}", body);
        try {
            final Map<String, List<String>> formMap = Parser.parseString(body);
            log.debug("form params: {}", formMap);
            request.setFormParams(formMap);
        } catch (ParseException e) {
            throw new FormParseException("Invalid form");
        }

        return request;
    }
}
