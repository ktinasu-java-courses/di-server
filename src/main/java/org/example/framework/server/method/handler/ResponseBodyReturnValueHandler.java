package org.example.framework.server.method.handler;

import org.example.framework.di.annotation.Autowired;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.http.MediaTypes;
import org.example.framework.server.annotation.ResponseBody;
import org.example.framework.server.controller.method.converter.HttpMessageConverter;
import org.example.framework.server.controller.method.handler.ReturnValueHandler;
import org.example.framework.server.exception.NoSupportingMessageConverter;
import org.example.framework.server.exception.UnsupportedParameterException;
import org.example.framework.server.http.RequestImpl;
import org.example.framework.server.http.Response;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

@Component
@Autowired
public class ResponseBodyReturnValueHandler implements ReturnValueHandler {
    private final List<HttpMessageConverter> messageConverters = new ArrayList<>();

    @Autowired
    public void setMessageConverters(final List<HttpMessageConverter> messageConverters) {
        this.messageConverters.addAll(messageConverters);
    }

    @Override
    public boolean supportsReturnType(final Method method) {
        return method.isAnnotationPresent(ResponseBody.class);
    }

    @Override
    public void handleReturnType(final Object returnValue, final Method method, final RequestImpl request, final Response response) throws Exception {
        if (!supportsReturnType(method)) {
            throw new UnsupportedParameterException(method.getName());
        }

        final Class<?> returnType = method.getReturnType();
        final MediaTypes mediaType = request.getAccept();

        for (final HttpMessageConverter messageConverter : messageConverters) {
            if (messageConverter.canWrite(returnType, mediaType)) {
                messageConverter.write(returnValue, mediaType, response);
                return;
            }
        }

        throw new NoSupportingMessageConverter(mediaType.value());
    }
}
