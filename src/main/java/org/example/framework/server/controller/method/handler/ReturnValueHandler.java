package org.example.framework.server.controller.method.handler;

import org.example.framework.server.http.RequestImpl;
import org.example.framework.server.http.Response;

import java.lang.reflect.Method;

public interface ReturnValueHandler {
    boolean supportsReturnType(final Method method);

    void handleReturnType(final Object returnValue, final Method method, final RequestImpl request, final Response response) throws Exception;
}
