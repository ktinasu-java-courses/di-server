package org.example.framework.server.controller.method.resolver;

import org.example.framework.server.http.RequestImpl;

import java.lang.reflect.Parameter;

public interface ArgumentResolver {
    boolean supportsParameter(final Parameter parameter);

    Object resolveArgument(final Parameter parameter, final RequestImpl request) throws Exception;
}
