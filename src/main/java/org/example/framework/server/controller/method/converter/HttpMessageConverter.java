package org.example.framework.server.controller.method.converter;

import org.example.framework.security.http.MediaTypes;
import org.example.framework.server.http.RequestImpl;
import org.example.framework.server.http.Response;

public interface HttpMessageConverter {
    boolean canRead(final Class<?> clazz, final MediaTypes mediaType);

    boolean canWrite(final Class<?> clazz, final MediaTypes mediaType);

    Object read(final Class<?> clazz, final RequestImpl request);

    void write(final Object object, final MediaTypes mediaType, final Response response);
}
