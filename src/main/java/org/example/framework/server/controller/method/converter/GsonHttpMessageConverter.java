package org.example.framework.server.controller.method.converter;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.http.MediaTypes;
import org.example.framework.server.http.RequestImpl;
import org.example.framework.server.http.Response;

import java.nio.charset.StandardCharsets;

@Component
@RequiredArgsConstructor
public class GsonHttpMessageConverter implements HttpMessageConverter {
  private final Gson gson;

  @Override
  public boolean canRead(final Class<?> clazz, final MediaTypes mediaType) {
    return MediaTypes.APPLICATION_JSON.equals(mediaType);
  }

  @Override
  public boolean canWrite(final Class<?> clazz, final MediaTypes mediaType) {
    return MediaTypes.APPLICATION_JSON.equals(mediaType);
  }

  @Override
  public Object read(final Class<?> clazz, final RequestImpl request) {
    return gson.fromJson(new String(request.getBody(), StandardCharsets.UTF_8), clazz);
  }

  @Override
  public void write(final Object object, final MediaTypes mediaType, final Response response) {
    response.write(mediaType, gson.toJson(object));
  }
}
