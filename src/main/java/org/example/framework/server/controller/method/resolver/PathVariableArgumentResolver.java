package org.example.framework.server.controller.method.resolver;

import lombok.RequiredArgsConstructor;
import org.example.framework.di.annotation.Component;
import org.example.framework.server.annotation.PathVariable;
import org.example.framework.server.exception.UnsupportedParameterException;
import org.example.framework.server.exception.UnsupportedParameterTypeException;
import org.example.framework.server.http.RequestImpl;

import java.lang.reflect.Parameter;

@Component
@RequiredArgsConstructor
public class PathVariableArgumentResolver implements ArgumentResolver {
    @Override
    public boolean supportsParameter(final Parameter parameter) {
        return parameter.isAnnotationPresent(PathVariable.class);
    }

    @Override
    public Object resolveArgument(final Parameter parameter, final RequestImpl request) throws Exception {
        if (!supportsParameter(parameter)) {
            throw new UnsupportedParameterException(parameter.getName());
        }
        final Class<?> paramClazz = parameter.getType();
        final PathVariable annotation = parameter.getAnnotation(PathVariable.class);

        final String value = request.getPathGroup(annotation.value());

        if (paramClazz.equals(long.class)) {
            return Long.parseLong(value);
        }

        if (paramClazz.equals(String.class)) {
            return value;
        }

        throw new UnsupportedParameterTypeException(parameter.getName());
    }
}
