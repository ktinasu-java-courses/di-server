package org.example.framework.server.exception;

public class InvalidControllerBeanException extends RuntimeException {
  public InvalidControllerBeanException() {
  }

  public InvalidControllerBeanException(String message) {
    super(message);
  }

  public InvalidControllerBeanException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidControllerBeanException(Throwable cause) {
    super(cause);
  }

  public InvalidControllerBeanException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
