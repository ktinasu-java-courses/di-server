package org.example.framework.server.exception;

public class BodyReadingException extends RuntimeException {
    public BodyReadingException() {
    }

    public BodyReadingException(String message) {
        super(message);
    }

    public BodyReadingException(String message, Throwable cause) {
        super(message, cause);
    }

    public BodyReadingException(Throwable cause) {
        super(cause);
    }

    public BodyReadingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
